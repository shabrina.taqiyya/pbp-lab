from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['To', 'From', 'Title', 'Message']

    error_messages = {
        'required' : 'Please Type'
    }
    input_to = {
        'type' : 'text',
        'placeholder' : 'To',
        'style' : 'width: 100%; padding: 10px 10px; box-sizing: border-box; border-radius: 4px; border : 1px solid #ccc;'
    }
    input_from = {
        'type' : 'text',
        'placeholder': 'From',
        'style' : 'width: 100%; padding: 10px 10px; box-sizing: border-box; border-radius: 4px; border : 1px solid #ccc;'
    }
    input_title = {
        'type' : 'text',
        'placeholder' : 'Title',
        'style' : 'width: 100%; padding: 10px 10px; box-sizing: border-box; border-radius: 4px; border : 1px solid #ccc;'
    }
    input_message = {
        'type' : 'text',
        'placeholder' : 'insert your message here',
        'style' : 'width: 100%; padding: 10px 10px; box-sizing: border-box; border-radius: 4px; border : 1px solid #ccc;'
    }

    To = forms.CharField(label='To ', required=False, max_length=30, widget=forms.TextInput(attrs=input_to))
    
    From = forms.CharField(label='From ', required=False, max_length=30, widget=forms.TextInput(attrs=input_from))
    
    Title = forms.CharField(label='Title ', required=False, max_length=30, widget=forms.TextInput(attrs=input_title))
    
    Message = forms.CharField(label='Message ', required=False, max_length=100, widget=forms.Textarea(attrs=input_message))