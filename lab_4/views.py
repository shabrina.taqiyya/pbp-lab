from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http import response

# Create your views here.

def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid):
            form.save()
            return response.HttpResponseRedirect('/lab-4')
    context['form'] = form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note' : note}
    return render(request, 'lab4_note_list.html', response)