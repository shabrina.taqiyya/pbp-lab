import 'package:flutter/material.dart';

class Task {
  final String title;
  final String description;
  final Checkbox checklist;
  final DateTime deadline;

  Task({
    required this.title,
    required this.description,
    required this.checklist,
    required this.deadline,
});
}