import 'package:flutter/material.dart';
import './task.dart';

class Tasks {
  final String title;
  List<Task> task = [];

  Tasks({
    required this.title,
  });
}
