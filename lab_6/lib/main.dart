import 'package:flutter/material.dart';
import 'package:lab_6/dummy_data.dart';
import 'package:lab_6/models/task.dart';
import 'package:lab_6/widgets/Name.dart';
import 'package:lab_6/widgets/Search.dart';
import 'package:lab_6/widgets/Todolist.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To Do List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'To Do List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.brown),
        ),
        backgroundColor: Colors.yellow,
      ),
      body: Container(
        margin: EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child:
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Name(),
              SizedBox(
                height: 20,
              ),
              Search(),
              TodoList(task: DUMMY_TASK)
            ],
          ),
        ),
      ),
    );
  }
}
