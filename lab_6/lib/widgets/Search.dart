import 'package:flutter/material.dart';
// https://docs.flutter.dev/release/breaking-changes/buttons
class Search extends StatelessWidget {

  @override
  Widget build(BuildContext) {
    return Column(
      children: [
        Container(
          child: TextField(
            decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search Task.dart'
            ),
          ),
        ),
        TextButton(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.brown),
              overlayColor: MaterialStateProperty.resolveWith<Color?> (
                  (Set<MaterialState> states) {
                    if (states.contains(MaterialState.hovered))
                      return Colors.blue.withOpacity(0.04);
                    if (states.contains(MaterialState.focused) ||
                        states.contains(MaterialState.pressed))
                      return Colors.blue.withOpacity(0.12);
                    return null;
                  }
              )
          ),
            onPressed: () {},
            child: Text('Search')),
      ],
    );
  }
}
