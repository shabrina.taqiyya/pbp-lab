import 'package:flutter/material.dart';
import 'package:lab_6/models/tasks.dart';
import '../dummy_data.dart';

class TodoList extends StatelessWidget {
  List<Tasks> task = [];
  TodoList({Key? key, required this.task}) : super(key: key);

  // https://api.flutter.dev/flutter/widgets/ListView-class.html
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: task.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 50,
          child: Center(child: Text('${DUMMY_TASK[index]}')),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}