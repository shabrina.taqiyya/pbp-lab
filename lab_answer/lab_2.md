1. Apakah perbedaan antara JSON dan XML?
    JSON (JavaScript Object Notation) lebih sederhana dan mudah dibaca, karena data pada JSON disimpan dalam bentuk key dan value. Sedangkan XML (eXtensible Markup Language) lebih rumit, karena disimpan sebagai tree structure. 

    Secara bahasa, XML merupakan bahasa markup yang memiliki tag untuk mendefinisikan elemen sedangkan JSON merupakan format yang ditulis dalam JavaScript. XML mendukung namespaces sedangkan JSON tidak. Ukuran dokumen XML besar dan oleh karenanya, struktur tag membuatnya besar dan lebih rumit untuk dibaca sedangkan ukuran dokumen JSON lebih ringkas dan mudah dibaca, tidak ada data yang tidak terpakai membuat file terlihat sederhana.

2. Apakah perbedaan antara HTML dan XML?
    XMl (eXtensible Markup Language) dan HTML (HyperText Markup Language) sama-sama merupakan bahasa markup namun memiliki beberapa perbedaan. XML lebih berfokus pada transfer data sedangkan HTML fokus pada penyajian data. XML menyediakan dukungan namespaces sedangkan HTML tidak. Tag pada XML dapat dikembangkan sedangkan tag pada HTML terbatas. 