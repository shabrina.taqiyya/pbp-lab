import 'package:flutter/material.dart';
import 'package:lab_7/models/task.dart';

class TodoForm extends StatefulWidget {
  @override
  _TodoForm createState() => _TodoForm();
}

class _TodoForm extends State<TodoForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController dateinput = TextEditingController();
  bool checkBoxValue = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "title",
                  labelText: "Title",
                  icon: Icon(Icons.circle),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'Title is required';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "description",
                  labelText: "Description",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
              ),
              Checkbox(
                checkColor: Colors.white,
                value: checkBoxValue,
                onChanged: (bool? value) {
                  setState(() {
                    checkBoxValue = value!;
                  });
                },
              ),
              // TextField(
              //   controller: dateinput, //editing controller of this TextField
              //   decoration: InputDecoration(
              //       icon: Icon(Icons.calendar_today), //icon of text field
              //       labelText: "Enter Date" //label text of field
              //   ),
              //   readOnly: true,
              //   onTap: () async {
              //     DateTime selectedTime = await showDatePicker(
              //         context: context,
              //         initialDate: DateTime.now(),
              //         firstDate: DateTime(2000),
              //         lastDate: DateTime(2100),
              //     );
              //   }
              // ),
              ElevatedButton(
                onPressed: () {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Processing Data')),
                    );
                  }
                },
                child: const Text('Submit'),
              ),
            ],
          ),
        )
      ],
    );
  }
}
