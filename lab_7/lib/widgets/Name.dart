import 'package:flutter/material.dart';

class Name extends StatelessWidget {
  final TextStyle name = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  final TextStyle taskNum = TextStyle(fontSize: 20);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Text(
                "Hello ",
                style: name,
              ),
              Text(
                "Binaaa",
                style: name,
              )
            ],
          ),
        ),

        Row(
          children: <Widget>[
            Text(
              "You have 2 ",
              style: taskNum,
            ),
            Text("incomplete task", style: taskNum,)
          ],
        ),
      ],
    );
  }
}
