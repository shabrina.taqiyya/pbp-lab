import 'package:flutter/material.dart';

class Task {
  TextField title;
  TextField description;
  bool checklist = false;
  TextField deadline;

  Task({
    required this.title,
    required this.description,
    required this.deadline,
});
}